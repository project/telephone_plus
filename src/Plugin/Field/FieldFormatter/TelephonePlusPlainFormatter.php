<?php

namespace Drupal\telephone_plus\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telephone_plus\TelephonePlusFormatter;
use Drupal\telephone_plus\TelephonePlusValidator;

/**
 * Plugin implementation of the 'telephone_plus_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "telephone_plus_plain",
 *   label = @Translation("Plain text (deprecated)"),
 *   field_types = {
 *     "telephone_plus_field"
 *   }
 * )
 *
 * @deprecated in telephone_plus:2.x. Please update view modes to use the telephone_plus field formatter.
 *
 */
class TelephonePlusPlainFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $vcard = FALSE;
    $as_link = FALSE;

    foreach ($items as $delta => $item) {
      $telephone = new TelephonePlusValidator($item->telephone_number, $item->telephone_extension, $item->country_code);

      // If we don't have a valid number, set variables to allow fallback to
      // plain text.
      if (!$telephone->isValid()) {
        $telephone_text = $item->telephone_number;
        $telephone_link = '';
      }
      else {
        $telephone = new TelephonePlusFormatter($item->telephone_number, $item->telephone_extension, $item->country_code);

        // TelephonePlus link text.
        $telephone_link = $as_link ? $telephone->url() : '';
        // TelephonePlus display text.
        $telephone_text = $telephone->text($item->display_international_number);
      }

      $elements[$delta] = [
        '#theme' => 'telephone_plus_item',
        '#title' => $item->telephone_title,
        '#number' => $telephone_text,
        '#url' => $telephone_link,
        '#extension' => $item->telephone_extension,
        '#supplementary' => $item->telephone_supplementary,
        '#vcard' => $vcard,
      ];
    }

    // If vCard option is enabled wrap all the elements with the required
    // class and include the microformat link.
    if ($vcard) {
      $elements['#attributes']['class'][] = 'vcard';
      $elements['#attached']['html_head_link'][] = [
        [
          'rel' => 'profile',
          'href' => 'http://microformats.org/profile/hcard',
        ],
      ];
    }

    return $elements;
  }

}
