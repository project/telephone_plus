<?php

namespace Drupal\telephone_plus;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;

/**
 * Class to help formatting telephone numbers to links and text.
 */
class TelephonePlusFormatter {

  /**
   * Phone Number Util definition.
   *
   * @var \libphonenumber\PhoneNumberUtil
   */
  protected $phoneNumberUtil;

  /**
   * Phone Number.
   *
   * @var string
   */
  protected $number;

  /**
   * Extension number.
   *
   * @var string
   */
  protected $extension;

  /**
   * A 2 letter country code.
   *
   * See: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.
   *
   * @var string
   */
  protected $countryCode;

  /**
   * Phone number object.
   *
   * @var \libphonenumber\PhoneNumber
   */
  protected $telephone;

  /**
   * TelephonePlusFormatter constructor.
   *
   * @param string $number
   *   Telephone number.
   * @param string $extension
   *   Telephone extension number.
   * @param string $country_code
   *   A 2 letter country code.
   */
  public function __construct($number, $extension, $country_code) {
    $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
    $this->number = $number;
    $this->extension = $extension;
    $this->countryCode = $country_code;
  }

  /**
   * Generate tel: protocol link including extension if present.
   *
   * @return string
   *   URL
   * @throws NumberParseException
   */
  public function url() {
    $dialing_number = $this->number;
    if (!empty($this->extension)) {
      $dialing_number .= '#' . $this->extension;
    }

    try {
      $telephone = $this->phoneNumberUtil->parse($dialing_number, $this->countryCode);
    }
    catch (NumberParseException $e) {
      throw $e;
    }

    return $this->phoneNumberUtil->format($telephone, PhoneNumberFormat::RFC3966);
  }

  /**
   * Display dialing number in either national or international format.
   *
   * @param bool $display_international_format
   *   Display number with internation dialing prefix.
   *
   * @return string
   *   Telephone number text.
   */
  public function text($display_international_format = FALSE) {
    $dialing_number = $this->number;
    if (!empty($this->extension)) {
      $dialing_number .= '#' . $this->extension;
    }

    $telephone = $this->phoneNumberUtil->parse($dialing_number, $this->countryCode);

    if ($display_international_format) {
      return $this->phoneNumberUtil->format($telephone, PhoneNumberFormat::INTERNATIONAL);
    }
    else {
      return $this->phoneNumberUtil->format($telephone, PhoneNumberFormat::NATIONAL);
    }
  }

}
