<?php

namespace Drupal\telephone_plus;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

/**
 * Class to validate telephone numbers.
 */
class TelephonePlusValidator {

  /**
   * Phone Number Util definition.
   *
   * @var \libphonenumber\PhoneNumberUtil
   */
  protected $phoneNumberUtil;

  /**
   * Phone Number.
   *
   * @var string
   */
  protected $number;

  /**
   * Extension number.
   *
   * @var string
   */
  protected $extension;

  /**
   * A 2 letter country code.
   *
   * See: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2.
   *
   * @var string
   */
  protected $countryCode;

  /**
   * Phone number object.
   *
   * @var \libphonenumber\PhoneNumber
   */
  protected $telephone;

  /**
   * TelephonePlusValidator constructor.
   *
   * @param string $number
   *   Telephone number.
   * @param string $extension
   *   Telephone extension number.
   * @param string $country_code
   *   A 2 letter country code.
   */
  public function __construct($number, $extension, $country_code) {
    $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
    $this->number = $number;
    $this->extension = $extension;
    $this->countryCode = $country_code;
  }

  /**
   * Check if the telephone number is valid.
   *
   * @return bool
   *   Return TRUE if telephone number is valid.
   */
  public function isValid() {
    try {
      $dialing_number = $this->number;
      if (!empty($this->extension)) {
        $dialing_number .= '#' . $this->extension;
      }

      $this->phoneNumberUtil->parse($dialing_number, $this->countryCode);
    } catch (NumberParseException $e) {
      return FALSE;
    }

    return TRUE;
  }
}
