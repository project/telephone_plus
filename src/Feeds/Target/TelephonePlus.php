<?php

namespace Drupal\telephone_plus\Feeds\Target;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Defines a telephone plus field mapper.
 *
 * @FeedsTarget(
 *   id = "telephone_plus",
 *   field_types = {"telephone_plus_field"}
 * )
 */
class TelephonePlus extends FieldTargetBase {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FieldTargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('telephone_title')
      ->addProperty('country_code')
      ->addProperty('telephone_number')
      ->addProperty('telephone_extension')
      ->addProperty('telephone_supplementary')
      ->addProperty('display_international_number');
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    if (empty($values['display_international_number'])) {
      $values['display_international_number'] = FALSE;
    }
  }

}
