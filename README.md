# Telephone Plus field

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
- [Additional information](#additional-information)

## Introduction

The Telephone Plus module provides a more descriptive way of displaying telephone information compared to the core telephone field.

The field provides the following additions:

- Label (optional)
- Extension (optional)
- Supplementary information (optional)
- Country code

The field can be displayed as plain text or a link, with the international dialing code and with or without [hcard](http://microformats.org/wiki/hcard) markup for telephone numbers.

Use cases for this module include:

- Providing labels for multiple telephone lines (e.g. Sales, Customer support, Technical support).
- Extension numbers for when a telephone line supports multiple points of contact.
- Adding supplemental details (e.g. call between 9am - 8pm, Mon-Fri).
- Provide both local and international dialing numbers for the same telephone line.

## Requirements

This module requires the [giggsey/libphonenumber-for-php-lite](giggsey/libphonenumber-for-php-lite) library which will automatically be included if this module
is installed with composer.

## Installation

Install using any of the methods described on the Drupal.org page [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).

## Usage

To use this module add a new field of type 'Telephone Plus' to your entity.
Configure field settings to allow titles and supplementary information fields.

The field formatter can be configured to display the field as plain text or as a link (with the tel: protocol)
and include the markup for the hcard microformat.

## Additional information

- http://microformats.org/wiki/hcard
- https://github.com/giggsey/libphonenumber-for-php-lite

